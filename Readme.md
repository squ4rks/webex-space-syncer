# Space Syncer

A simple REST interface to asynchronously sync space members using flask and rq.

## Setup 

1. Install the requirements
```
$ python3 -m pip install -r requirements.txt
```
2. Export your `WEBEX_TOKEN` (and other possible configuration variables - see below)
```
$ export WEBEX_TOKEN="<INSERT YOUR WEBEX TOKEN HERE>"
```
3. Start the web server (only use the development server for testing - not production)
```
$ python3 app.py
```
4. Open a new terminal window/tab. Repeat step 2 in the new window/tab. Then start the worker.
```
$ rq worker default
```
5. Done - You can now send requests to the server on port 7073.

## Configuration Variables

**Required**

* `WEBEX_TOKEN` - The token of the Webex bot used to create and sync the spaces.

**Optional**

* `REDIS_HOST` - The host of your redis server (default: `127.0.0.1`)
* `REDIS_PORT` - The port of your redis server (default: `6379`)