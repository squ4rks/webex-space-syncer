# Copyright (c) 2022 Cisco and/or its affiliates.
# 
# This software is licensed to you under the terms of the Cisco Sample
# Code License, Version 1.1 (the "License"). You may obtain a copy of the
# License at
#
#               https://developer.cisco.com/docs/licenses
#
# All use of the material herein must be in accordance with the terms of
# the License. All rights not expressly granted by the License are
# reserved. Unless required by applicable law or agreed to separately in
# writing, software distributed under the License is distributed on an "AS
# IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
# or implied.

import os
from webexteamssdk import WebexTeamsAPI
from rq import Queue
from redis import Redis

REDIS_HOST = os.environ.get("REDIS_HOST", "127.0.0.1")
REDIS_PORT = int(os.environ.get("REDIS_PORT", 6379))

api = WebexTeamsAPI(access_token=os.environ.get('WEBEX_TOKEN'))
me = api.people.me()
queue = Queue(connection=Redis(host=REDIS_HOST, port=REDIS_PORT))

def delete_membership(space_id, mail):
    for mem in api.memberships.list(roomId=space_id, personEmail=mail):
        api.memberships.delete(mem.id)

def add_membership(space_id, mail):
    mem = api.memberships.create(roomId=space_id, personEmail=mail)

def sync_memberships(space_id, members):
    # Append bot to keep himself in members all the time
    members.append(me.emails[0])

    # Determine current members
    to_delete = []
    to_add = []

    current_members = set()
    for mem in api.memberships.list(roomId=space_id):
        current_members.add(mem.personEmail)
    
    # Determine those that need to be deleted
    for mem in current_members:
        if mem not in members:
            to_delete.append(mem)
    
    # Determine those that need to be added
    for mem in members:
        if mem not in current_members:
            to_add.append(mem)

    ret = {
        'space_id': space_id,
        'delete': [],
        'add': []
    }
    for mail in to_delete:
        job = queue.enqueue(delete_membership, space_id=space_id, mail=mail)
        ret['delete'].append({'mail': mail, 'job': job.id})
    
    for mail in to_add:
        job = queue.enqueue(add_membership, space_id=space_id, mail=mail)
        ret['add'].append({'mail': mail, 'job': job.id})

    return ret