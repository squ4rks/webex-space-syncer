# Copyright (c) 2022 Cisco and/or its affiliates.
# 
# This software is licensed to you under the terms of the Cisco Sample
# Code License, Version 1.1 (the "License"). You may obtain a copy of the
# License at
#
#               https://developer.cisco.com/docs/licenses
#
# All use of the material herein must be in accordance with the terms of
# the License. All rights not expressly granted by the License are
# reserved. Unless required by applicable law or agreed to separately in
# writing, software distributed under the License is distributed on an "AS
# IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
# or implied.

import os

from flask import Flask, request, jsonify
from webexteamssdk import WebexTeamsAPI
from rq import Queue
from rq.job import Job
from redis import Redis

from tasks import add_membership, sync_memberships

REDIS_HOST = os.environ.get("REDIS_HOST", "127.0.0.1")
REDIS_PORT = int(os.environ.get("REDIS_PORT", 6379))

api = WebexTeamsAPI(access_token=os.environ.get('WEBEX_TOKEN'))
me = api.people.me()

app = Flask(__name__)
queue = Queue(connection=Redis(host=REDIS_HOST, port=REDIS_PORT))

@app.route('/rooms', methods=['GET', 'POST'])
def rooms_endpoint():
    if request.method == "GET":
        ret = []
        for space in api.rooms.list(type='group'):
            ret.append({'id': str(space.id), 'title': str(space.title)})

        return jsonify(ret)
    if request.method == "POST":
        data = request.get_json()
        space = api.rooms.create(title=data['title'])

        jobs = []
        for mail in data['members']:
            job = queue.enqueue(add_membership, space_id=space.id, mail=mail)

            jobs.append(job.id)
        
        return jsonify({'jobs': jobs, 'room_id': str(space.id)})

@app.route('/rooms/<space_id>')
def rooms_info_endpoint(space_id):
    space = api.rooms.get(roomId=space_id)

    ret = {
        'id': space.id,
        'title': space.title,
        'members': []
    }

    for mem in api.memberships.list(roomId=space.id):
        if mem.personId != me.id:
            ret['members'].append(mem.personEmail)

    return jsonify(ret)

@app.route('/rooms/<space_id>/sync', methods=['POST'])
def rooms_sync_endpoint(space_id):
    space = api.rooms.get(roomId=space_id)

    data = request.get_json()

    job = queue.enqueue(sync_memberships, space_id=space_id, members=data['members'])

    return jsonify({'job_id': job.id})

@app.route('/jobs/<job_id>')
def jobs_status_endpoint(job_id):
    job = Job.fetch(id=job_id, connection=Redis())

    ret = {
        'job_id': job.id,
        'status': job.get_status(),
        'meta': job.get_meta(),
        'result': job.result
    }

    return ret

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=7073)